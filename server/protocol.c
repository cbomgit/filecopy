//server protocol implementation file
#include "protocol.h"

void get_file_list(char **data) {

   DIR *d;
   struct dirent *dir;
   int file_count = 0, len;
   int max_length = NAME_MAX, bytes_written;

   d = opendir(".");
   *data = (char*) malloc(max_length * sizeof(char*));

   if(d) {
      while((dir = readdir(d)) != NULL) {
         if(dir->d_type == DT_REG) {
            if(sizeof(char*) + bytes_written + (strlen(dir->d_name)*sizeof(char*)) > max_length){
               max_length *= 2;
               *data = (char*) realloc(*data, max_length * sizeof(char*));
            }
            bytes_written += strlen(dir->d_name) * sizeof(char*) + sizeof(char*);
            strcat(*data, dir->d_name);
            strcat(*data, " ");
         }
      }
   }
 
}

void send_listing(int socket) {

   int len, status;
   char *list;
   char msg[] = "Directory listing not available.";
   uint32_t length;

   get_file_list(&list);

   //directory may be empty or there was an error in get_file_list
   //default to another message
   if(list == NULL || strlen(list) == 0) {
      list = msg;
   }

   len = strlen(list);
   length = htonl(len);

   //send the length of the string we are about to send
   status = send(socket, &length, sizeof(uint32_t), 0);
   if(status == -1) {
      perror("send()");
      exit(EXIT_FAILURE);
   }

   //then send the listing
   status = send_all(socket, list, &len);
   if(status == -1) {
      perror("send()");
      exit(EXIT_FAILURE);
   }

   free(list);

}

int check_file_exists(char *file) {

   DIR *d;
   struct dirent *dir;
   int file_count = 0, len;
   int max_length = NAME_MAX, bytes_written;

   d = opendir(".");

   if(d) {
      while((dir = readdir(d)) != NULL) {
         if(dir->d_type == DT_REG) {
            if(strcmp(dir->d_name, file) == 0)
               return 1;
         }
      }
   }
   return 0;
}

void dcopy(int socket, char *file) {

   struct stat st;
   int status, len;
   off_t offset;
   uint32_t packed;
   int fp;
   size_t size;

   //the client wants to download a file from 
   //us. Check to see if it even exists.
   if(file == NULL || strlen(file) == 0) {
      send_reply(socket, USAGE);
   }
   else if(!check_file_exists(file)) {
      send_reply(socket, NOT_FOUND);
   }
   else {
      //send reply indicating we are ready to send the file
      send_reply(socket, SEND);
      
      //send the size of the file
      stat(file, &st);
      size = st.st_size;
      packed = htonl(size);
      status = send(socket, &packed, sizeof(uint32_t), 0);
      if(status == -1) {
         perror("Network Error: Failed to send file size. Send()");
         exit(EXIT_FAILURE);
      }
      //followed by the length of the file name and the name of the file
      packed = htonl(strlen(file));
      status = send(socket, &packed, sizeof(uint32_t), 0);
      if(status == -1) {
         perror("Network Error: Failed to send file size. Send()");
         exit(EXIT_FAILURE);
      }
      len = strlen(file);
      status = send_all(socket, file, &len);
      if(status == -1) {
         perror("Network Error: Failed to send file size. Send()");
         exit(EXIT_FAILURE);
      }
      

      //now send the whole file
      fp = open(file, O_RDONLY);
      if(fp == -1) {
         fprintf(stderr,"Could not open file for reading.\n", strerror(errno));
         exit(EXIT_FAILURE);
      }
      offset = 0;
      status = sendfile(socket, fp, &offset, size);
      if(status == -1) {
         fprintf(stderr, "error from sendfile: %s\n", strerror(errno));
         exit(EXIT_FAILURE);
      }
      if(status != size) {
         fprintf(stderr, "Incomplete send.\n");
         exit(EXIT_FAILURE);
      }
      close(fp);
   }

}

void ucopy(int socket, char *file) {

   int bytes_recv, total_recv;
   int bytes_written, total_written;
   void *buffer, *start_addr;
   uint32_t int_buffer;
   int fd, status, len;
   int file_size;
   mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

   if(file == NULL || strlen(file) == 0) {
      send_reply(socket, USAGE);
   }
   else if(check_file_exists(file)) {
      send_reply(socket, DUPLICATE);
   }
   else {
      send_reply(socket, RECV);

      //send the length of the file name and the name of the file
      int_buffer = htonl(strlen(file));
      status = send(socket, &int_buffer, sizeof(uint32_t), 0);
      if(status == -1) {
         perror("Network Error: Failed to send file size. Send()");
         exit(EXIT_FAILURE);
      }
      len = strlen(file);
      status = send_all(socket, file, &len);
      if(status == -1) {
         perror("Network Error: Failed to send file size. Send()");
         exit(EXIT_FAILURE);
      }

      //get the file size
      bytes_recv = recv(socket, &int_buffer, sizeof(uint32_t), 0 );
      if(bytes_recv == -1) {
         exit(EXIT_FAILURE);
      }
      //allocate memory for buffer
      buffer = (void*) malloc(file_size * sizeof(void*));
      file_size = ntohl(int_buffer);
      start_addr = buffer;

      //now get the file
      fd = open(file, O_RDWR | O_CREAT, mode);
      if(fd == -1) {
         perror("Could not open file.");
         exit(EXIT_FAILURE);
      }

      while(total_recv < file_size) {
         bytes_recv = recv(socket, buffer + total_recv, file_size - total_recv, 0);
         if(bytes_recv == -1) {
            perror("Error receiving file");
            exit(EXIT_FAILURE);
         }
         total_recv += bytes_recv;
      }

      bytes_written = write(fd, buffer, file_size);
      if(bytes_written == -1) {
         perror("File could not be written");
         exit(EXIT_FAILURE);
      }

      if(bytes_written < file_size) {
         perror("File partially received");
         exit(EXIT_FAILURE);
      }

      close(fd);
   }

}
//if command isn't catalog or bye, then either
//user wants upload/download or they sent some
//other command and we need to respond accordingly
void process_command(char *arr, int socket) {

   char *cmd, *args;
   int i, j;
   int remaining;

   //split the msg in two - a command and an array
   i = 0;
   while(arr[i] && arr[i] != '\n' && arr[i] != ' ') {
      i++;
   }

   //allocate memory for the cmd array
   //and copy it over
   cmd = (char*) malloc(1 + i * sizeof(char*));

   for(j = 0; j < i; j++) {
      cmd[j] = arr[j];
   }

   cmd[j] = '\0';

   //allocate memory for the remaining array and copy it over
   remaining = strlen(arr) - i - 2;
   if(remaining > -1) {

      args = (char*) malloc(remaining + 1 * sizeof(char*));
      for(j = 0; arr[1 + i + j]; j++) {
         args[j] = arr[1 + i + j];
      }
      args[j] = '\0';
   }
   else {
      args = NULL;
   }

   //now process the command - at this point the command
   //can only be ucopy, dcopy, or some invalid command
   if(args == NULL || strlen(args) == 0) {
      send_reply(socket, USAGE);
   }
   else if(strcmp(cmd, "ucopy") == 0) {
      ucopy(socket, args);
   }
   else if(strcmp(cmd, "dcopy") == 0) {
      dcopy(socket, args);
   }
   else {
      send_reply(socket, INVALID_CMD);
   }

   if(remaining > -1)
      free(args);
   free(cmd);
 
}

void send_reply(int socket, int reply) {
   uint32_t msg = htonl(reply);
   int sent;
   sent = send(socket, &msg, sizeof(uint32_t), 0);
   if(sent == -1) {
      perror("Error sending reply to client. Aborting. send()");
      exit(EXIT_FAILURE);
   }
}

void fetch_cmd(int socket, char **recv_cmd) {

   //holds length of message. Passed over wire as 
   //uniform 4 bytes to get around platform dependent
   //implementation of itn
   uint32_t cmd_length;     

   //cmd_length converted to int, keep track of bytes received
   int len = 0, bytes_recv = 0; 

   //get the length of the message. Max of 263 bytes
   bytes_recv = recv(socket, &cmd_length, sizeof(uint32_t), 0);
   if(bytes_recv == -1) {
      perror("recv()");
      exit(1);
   }

   //conver to little endian and get the reset of the message
   len = ntohl(cmd_length);
   bytes_recv = recv_all(socket, recv_cmd, &len);
   if(bytes_recv == -1) {
      perror("recv()");
   }
}

int server_setup(struct addrinfo *hints, struct addrinfo **servinfo)
{

   int sockfd, status;
   int yes = 1;

   memset(hints, 0, sizeof(struct addrinfo));
   memset(servinfo, 0, sizeof(struct addrinfo));
   hints->ai_family = AF_INET;
   hints->ai_socktype = SOCK_STREAM;
   

   if ((status = getaddrinfo (LISTEN_ADDR, LISTEN_PORT, hints, servinfo)) != 0) {
      fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
      exit(1);
   }


   //get the file descriptor for the socket
   if((sockfd = socket((*servinfo)->ai_family, (*servinfo)->ai_socktype,(*servinfo)->ai_protocol)) == -1) {
      perror("socket()");
      exit(1);
   }

   //allow the port to be resued
   if(setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
      perror("setsockopt()");
      exit(EXIT_FAILURE);
   }

   //bind the socket to an IP.
   if(bind(sockfd, (*servinfo)->ai_addr, (*servinfo)->ai_addrlen) == -1) {
      perror("bind()");
      exit(1);
   }

   //listen on address and wait for connections
   if(listen(sockfd, BACKLOG) == -1) {
      perror("listen()");
      exit(1);
   }
   

   return sockfd;
}

//function that attempts to receive data over a socket
//until all data has been sent. 
//int socket - socket file descriptor
//char **data - ptr to an array. Needs to be a ptr to a ptr
//because we will change the value of the ptr. :)
//int *len - ptr to integer representing expected length of message
int recv_all(int socket, char **data, int *len) {

   int total_recv = 0;
   int bytes_recv = 0;

   while(total_recv < *len) {
      bytes_recv = recv(socket, data[total_recv], *len - total_recv, 0);
      if(bytes_recv == -1 || bytes_recv == 0)
         break;
      total_recv += bytes_recv;
   }

   (*data)[total_recv] = '\0';
   return 0;
}

int send_all(int socket, char *buff, int *len) {

   int total_sent = 0, bytes_sent = 0;
   while(total_sent < *len) {

      bytes_sent = send(socket, buff + total_sent, *len - total_sent, 0);
      if(bytes_sent == -1) {
         perror("send()");
         break;
      }
      total_sent += bytes_sent;
   }

   return bytes_sent == -1 ? -1 : 0;
}
