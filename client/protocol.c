#include "protocol.h"
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

void dir() {
      DIR *d;
      struct dirent *dir;
      int file_count = 0, len;

      d = opendir(".");

      if(d) {
         while((dir = readdir(d)) != NULL) {
            if(dir->d_type == DT_REG) {
               printf("%s ", dir->d_name);
            }
         }
      }

}

void get_listing(int socket) {

   
   //holds length of message. Passed over wire as 
   //uniform 4 bytes to get around platform dependent
   //implementation of itn
   uint32_t cmd_length;     
   char *data;

   //cmd_length converted to int, keep track of bytes received
   int len = 0, bytes_recv = 0; 

   //get the length of the message. Max of 263 bytes
   bytes_recv = recv(socket, &cmd_length, sizeof(uint32_t), 0);
   if(bytes_recv == -1) {
      perror("recv()");
      exit(1);
   }
   //conver to little endian and get the reset of the message
   len = ntohl(cmd_length);

   data = (char *) malloc(len * sizeof(char*));
   bytes_recv = recv_all(socket, &data, &len);
   if(bytes_recv == -1) {
      perror("recv()");
   }

   printf("%s\n", data);
   free(data);

}

void process_reply(int reply, int socket) {

   //Here we process the reply from the server

   if(reply == INVALID_CMD)
      usage();
   else if(reply == NOT_FOUND) {
      printf("File could not be found on the server for download.\n");
   }
   else if(reply == DUPLICATE) {
      printf("This file is already present on the server.\n");
   }
   else if(reply == USAGE) {
      usage();
   }
   else if(reply == SEND) {
      dcopy(socket);
   }
   else if(reply == RECV) {
      ucopy(socket);
   }
   else if(reply == BYE) {
      printf("Closing connection with server.\n");
   }
   else if(reply == LIST) {
      get_listing(socket);
   }
}

int fetch_reply(int socket) {
   int bytes_recv;
   uint32_t reply_t;

   bytes_recv = recv(socket, &reply_t, sizeof(uint32_t), 0);
   if(bytes_recv == -1) {
      perror("Error receiving reply. Aborting. recv()");
      exit(EXIT_FAILURE);
   }

   return ntohl(reply_t);
}

void usage() {
   printf("Commands: catalog\n");
   printf("          ucopy [file name]\n");
   printf("          dcopy [file name]\n");
   printf("          bye\n");

}

int send_cmd_length(int socket, int len) {

   uint32_t length = htonl(len);
   return send(socket, &length, sizeof(uint32_t), 0);

}

void dcopy(int socket) {

   int file_size, name_length, bytes_recv, total_recv;

   int bytes_written, total_written;
   char *file_name;
   void *buffer, *start_addr;
   uint32_t int_buffer;
   int fd;
   int i;
   mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
   //first get the file size
   bytes_recv = recv(socket, &int_buffer, sizeof(uint32_t), 0 );
   if(bytes_recv == -1) {
      exit(EXIT_FAILURE);
   }
   //allocate memory for buffer
   buffer = (void*) malloc(file_size * sizeof(void*));
   start_addr = buffer;
   file_size = ntohl(int_buffer);
   //now get the file name length
   bytes_recv = recv(socket, &int_buffer, sizeof(uint32_t), 0);
   if(bytes_recv == -1) {
      exit(EXIT_FAILURE);
   }
   name_length = ntohl(int_buffer);
   file_name = (char*) malloc(name_length * sizeof(char*));

   //get the file name
   bytes_recv = recv_all(socket, &file_name, &name_length);
   if(bytes_recv == -1) {
      perror("Could not get file name.");
      exit(EXIT_FAILURE);
   }

   //now get the file
   fd = open(file_name, O_RDWR | O_CREAT, mode);
   if(fd == -1) {
      perror("Could not open file.");
      exit(EXIT_FAILURE);
   }

   //get the whole file
   while(total_recv < file_size) {
      bytes_recv = recv(socket, buffer + total_recv, file_size - total_recv, 0);
      if(bytes_recv == -1) {
         perror("Error receiving file");
         exit(EXIT_FAILURE);
      }
      total_recv += bytes_recv;
   }

   bytes_written = write(fd, buffer, file_size);
   if(bytes_written == -1) {
      perror("File could not be written");
      exit(EXIT_FAILURE);
   }

   if(bytes_written < file_size) {
      perror("File partially received");
      exit(EXIT_FAILURE);
   }

   close(fd);
   free(file_name);
}

int send_all(int socket, char *buff, int *len) {

   int total_sent = 0, bytes_sent = 0;
   while(total_sent < *len) {
      bytes_sent = send(socket, buff + total_sent, *len - total_sent, 0);
      if(bytes_sent == -1) {
         perror("send()");
         break;
      }
      total_sent += bytes_sent;
   }

   return bytes_sent == -1 ? -1 : 0;
}

void ucopy(int socket) {

   struct stat st;
   int status, name_length, bytes_recv;
   off_t offset;
   uint32_t packed;
   int fp;
   size_t size;
   char *file_name;

   //get the file name length and file name
   bytes_recv = recv(socket, &packed, sizeof(uint32_t), 0);
   if(bytes_recv == -1) {
      exit(EXIT_FAILURE);
   }
   name_length = ntohl(packed);
   file_name = (char*) malloc(name_length * sizeof(char*));

   //get the file name
   bytes_recv = recv_all(socket, &file_name, &name_length);
   if(bytes_recv == -1) {
      perror("Could not get file name.");
      exit(EXIT_FAILURE);
   }

   //stat the file and send the size to the server
   //send the size of the file
   stat(file_name, &st);
   size = st.st_size;
   packed = htonl(size);
   status = send(socket, &packed, sizeof(uint32_t), 0);
   if(status == -1) {
      perror("Network Error: Failed to send file size. Send()");
      exit(EXIT_FAILURE);
   }

   printf("Sending %s\n", file_name);

   //now send the whole file
   fp = open(file_name, O_RDONLY);
   if(fp == -1) {
      fprintf(stderr,"Could not open file for reading.\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   offset = 0;
   status = sendfile(socket, fp, &offset, size);
   if(status == -1) {
      fprintf(stderr, "error from sendfile: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
   }
   if(status != size) {
      fprintf(stderr, "Incomplete send.\n");
      exit(EXIT_FAILURE);
   }
   close(fp);

}

//function that attempts to receive data over a socket
//until all data has been sent. 
//int socket - socket file descriptor
//char **data - ptr to an array. Needs to be a ptr to a ptr
//because we will change the value of the ptr. :)
//int *len - ptr to integer representing expected length of message
int recv_all(int socket, char **data, int *len) {

   int total_recv = 0;
   int bytes_recv = 0;

   while(total_recv < *len) {
      bytes_recv = recv(socket, *data + total_recv, *len - total_recv, 0);
      if(bytes_recv == -1 || bytes_recv == 0)
         break;
      total_recv += bytes_recv;
   }

   (*data)[total_recv] = '\0';
   return 0;
}