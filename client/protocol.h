//protocol header file

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdint.h>
#include <dirent.h>
#include <sys/stat.h>



#define CMD_MAX     8
#define MAX_INPUT_SIZE NAME_MAX + CMD_MAX
#define CMD_REPLY 3

#define INVALID_CMD 0 //Command not found on server
#define RECV        1 //client should prepare to receive data 
#define SEND        2 //server is waiting to received data
#define USAGE       3 //command was not called with proper arguments
#define NOT_FOUND   4 //file was not found on the server
#define DUPLICATE   5 //file already exists ont the server
#define BYE         6 //client wants to close the connection with the server
#define LIST        7 //the server will send a listing of the files in the current directory

int send_all(int socket, char *buff, int *len);
int send_cmd_length(int socket, int length);
void usage();
int fetch_reply(int socket);
void process_reply(int reply, int socket);
int recv_all(int socket, char **data, int *len);
void dcopy(int socket);
void ucopy(int socket);
void dir();