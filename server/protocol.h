//Server protocl header file

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//maximum number of queued connections
#define BACKLOG 10

//server parameters
#define LISTEN_ADDR "0.0.0.0"
#define LISTEN_PORT "34900"
#define MAX_CMD_SIZE 263

#define INVALID_CMD 0 //Command not found on server
#define RECV        1 //client should prepare to receive data
#define SEND        2 //server is waiting to received data
#define USAGE       3 //command was not called with proper arguments
#define NOT_FOUND   4 //file was not found on the server
#define DUPLICATE   5 //file already exists ont the server
#define BYE         6 //client wants to close the connection
#define LIST        7

int server_setup(struct addrinfo *hints, struct addrinfo **servinfo);
void catalog(int socket);
int send_all(int socket, char *buff, int *len); 
int recv_all(int socket, char **data, int *len);
void fetch_cmd(int socket, char **recv_cmd);
void send_reply(int socket, int reply);
void process_command(char *arr, int socket);
int check_file_exists(char *file);
void dcopy(int socket, char *file);
void ucopy(int socket, char *file);
void send_listing(int socket);
void get_file_listing(char **data);