#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "protocol.h"

int main(int argc, char *argv[])
{
   struct addrinfo hints, *res, *p;
   int status,sockfd,len,bytes_sent;
   char input[MAX_INPUT_SIZE];
   char ipstr[INET6_ADDRSTRLEN];
   

   if(argc != 3) {
      fprintf(stderr, "usage: client hostname port\n");
      return 1;
   }

   memset(&hints, 0, sizeof hints);
   hints.ai_family = AF_INET; 
   hints.ai_socktype = SOCK_STREAM;

   if((status = getaddrinfo("127.0.0.1", "34900", &hints, &res)) != 0) {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
      return 2;
   }
   
   sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
   
   if(sockfd == -1) {
      perror("socket()");
      exit(EXIT_FAILURE);
   }

   if(connect(sockfd, res->ai_addr, res->ai_addrlen) == -1) {
      perror("connect()");
      exit(1);
   }

   //first, get input from the user
   while(strcmp(input, "bye\n") != 0) {

      fgets(input, MAX_INPUT_SIZE, stdin);

      if(strcmp(input, "dir\n") == 0 ){
         dir();
      }
      else {

         //get the length of the data that will be sent. Basically, count until the appended newline
         len = strlen(input) - 1;
         bytes_sent =send_cmd_length(sockfd, len );
         if(bytes_sent == -1) {
            perror("Error sending data. Aborting. send()");
            exit(EXIT_FAILURE);
         }

         //now send the input
         bytes_sent=send_all(sockfd, input, &len);
         if(bytes_sent == -1) {
            perror("Error sending data. Aborting. send()");
            exit(EXIT_FAILURE);
         }

         //wait for a reply
         status = fetch_reply(sockfd);
         process_reply(status, sockfd);
      }
   }

   close(sockfd);
   freeaddrinfo(res);
   return 0;
}

