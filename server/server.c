#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <unistd.h>
#include "protocol.h"

int main() {

   int status, sockfd, client_sockfd;
   // int len, bytes_sent, bytes_recv;
   struct addrinfo hints;
   struct addrinfo *servinfo; 
   struct sockaddr_storage client_addr;
   socklen_t addr_size;
   char *recv_cmd;
   char reply[3];
   // uint32_t *cmd_length;

   //function call to get socket descriptor and listen on server address and port
   sockfd = server_setup(&hints, &servinfo);

   //set up client connection
   addr_size = sizeof client_addr;
   client_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, &addr_size);
    
   if(client_sockfd == -1) {
      perror("accept()");
      exit(1);
   }

   //allocate the msg buffer
   recv_cmd = (char *) malloc(sizeof(char) * MAX_CMD_SIZE);
   //start listening for a command
   fetch_cmd(client_sockfd, &recv_cmd);

   while(strcmp(recv_cmd, "bye") != 0) {
      //now we need to parse the command. May have an arg.
      //if no command is found, return a message saying so.
      //otherwise return "ok"; 

      if(strlen(recv_cmd) == 0) {
         send_reply(client_sockfd, INVALID_CMD);
      }
      else if(strcmp(recv_cmd, "catalog") == 0) {
         send_reply(client_sockfd, LIST);
         send_listing(client_sockfd);
      } 
      else {
         process_command(recv_cmd, client_sockfd);
      }

      fetch_cmd(client_sockfd, &recv_cmd);
   }

   send_reply(client_sockfd, BYE);
   //do some clean up
   close(sockfd);
   close(client_sockfd);
   freeaddrinfo(servinfo);
   free(recv_cmd);
   return 0;

}


